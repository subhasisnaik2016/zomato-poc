"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var helpers_1 = __importDefault(require("../helpers"));
var BaseRepository = /** @class */ (function () {
    function BaseRepository(response, dbModel, databaseManager) {
        this.appHelper = new helpers_1.default();
        this.dbModel = dbModel;
        this.databaseManager = databaseManager;
        this.response = response;
    }
    BaseRepository.prototype.findAll = function () { };
    BaseRepository.prototype.findOne = function (where) {
        return __awaiter(this, void 0, void 0, function () {
            var error, user, e_1;
            var _a;
            return __generator(this, function (_b) {
                switch (_b.label) {
                    case 0:
                        _b.trys.push([0, 2, , 3]);
                        error = void 0, user = void 0;
                        return [4 /*yield*/, this.appHelper.to(this.dbModel.findOne({
                                where: where
                            }))];
                    case 1:
                        _a = _b.sent(), error = _a[0], user = _a[1];
                        if (error) {
                            this.sendErrorResponse(error);
                        }
                        if (!user) {
                            return [2 /*return*/, { success: false }];
                        }
                        return [2 /*return*/, { success: true, data: user }];
                    case 2:
                        e_1 = _b.sent();
                        return [2 /*return*/, { success: false }];
                    case 3: return [2 /*return*/];
                }
            });
        });
    };
    BaseRepository.prototype.create = function (data) {
        return __awaiter(this, void 0, void 0, function () {
            var error, success, e_2;
            var _a;
            return __generator(this, function (_b) {
                switch (_b.label) {
                    case 0:
                        _b.trys.push([0, 2, , 3]);
                        error = void 0, success = void 0;
                        return [4 /*yield*/, this.dbModel.create(data)];
                    case 1:
                        _a = _b.sent(), error = _a[0], success = _a[1];
                        if (error) {
                            this.sendErrorResponse(error);
                        }
                        if (error) {
                            return [2 /*return*/, { success: false }];
                        }
                        return [2 /*return*/, { success: true, data: success }];
                    case 2:
                        e_2 = _b.sent();
                        return [2 /*return*/, { success: false }];
                    case 3: return [2 /*return*/];
                }
            });
        });
    };
    BaseRepository.prototype.update = function (id) { };
    BaseRepository.prototype.delete = function (id) { };
    BaseRepository.prototype.sendErrorResponse = function (error) {
        var app = this.response.app;
        var appHelper = app.globals.appHelpers;
        return appHelper.sendResponse(this.response, 500, "Something went wrong", {}, error);
    };
    return BaseRepository;
}());
exports.default = BaseRepository;
