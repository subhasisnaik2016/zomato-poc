"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var AppHelpers = /** @class */ (function () {
    function AppHelpers() {
        this.to = function (promise) {
            return promise
                .then(function (data) {
                return [null, data];
            })
                .catch(function (err) {
                return [err, null];
            });
        };
    }
    AppHelpers.prototype.sendResponse = function (res, statusCode, message, data, error) {
        if (message === void 0) { message = ""; }
        if (data === void 0) { data = {}; }
        if (statusCode >= 200 && statusCode < 400) {
            return res.status(statusCode).send({ status: true, message: message, data: data });
        }
        else {
            return res.status(statusCode).send({ status: false, message: message, data: data, error: error });
        }
    };
    ;
    return AppHelpers;
}());
exports.default = AppHelpers;
