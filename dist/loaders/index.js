"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var express_loader_1 = require("./express.loader");
var route_loader_1 = require("./route.loader");
var global_loader_1 = require("./global.loader");
var express = require("express");
var Loader = /** @class */ (function () {
    function Loader() {
        this.app = express();
        this.expressLoader = new express_loader_1.ExpressLoader(this.app);
        this.routeLoader = new route_loader_1.RouteLoader(this.app);
        this.globalLoader = new global_loader_1.GlobalLoader(this.app);
        this.init();
    }
    Loader.prototype.init = function () {
        this.expressLoader.load();
        this.routeLoader.load();
    };
    return Loader;
}());
exports.default = new Loader().app;
