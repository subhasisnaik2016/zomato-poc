"use strict";
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
    result["default"] = mod;
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
var bodyParser = __importStar(require("body-parser"));
var cors = require("cors");
var ExpressLoader = /** @class */ (function () {
    function ExpressLoader(app) {
        this.app = app;
    }
    /**
     * Configure application
     *
     * @class Server
     * @method config
     */
    ExpressLoader.prototype.load = function () {
        this.app.use(cors());
        //use json form parser middlware
        this.app.use(bodyParser.json());
        //use query string parser middlware
        this.app.use(bodyParser.urlencoded({
            extended: true,
            limit: "50mb"
        }));
    };
    return ExpressLoader;
}());
exports.ExpressLoader = ExpressLoader;
