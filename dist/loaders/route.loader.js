"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var rest_api_router_1 = __importDefault(require("../router/rest-api.router"));
var RouteLoader = /** @class */ (function () {
    function RouteLoader(app) {
        this.app = app;
    }
    /**
     * Configure application
     *
     * @class Server
     * @method config
     */
    RouteLoader.prototype.load = function () {
        this.app.use("/api/v1", new rest_api_router_1.default().router);
    };
    return RouteLoader;
}());
exports.RouteLoader = RouteLoader;
