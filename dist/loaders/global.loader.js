"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var databaseManager_1 = __importDefault(require("../databaseManager"));
var helpers_1 = __importDefault(require("../helpers"));
var GlobalLoader = /** @class */ (function () {
    function GlobalLoader(app) {
        this.app = app;
        app.globals = {};
        app.globals.databaseManager = new databaseManager_1.default();
        app.globals.appHelpers = new helpers_1.default();
        app.globals.secret = 'jkhjhsuihfweyr274y4f438gbhjsret84tgr';
    }
    return GlobalLoader;
}());
exports.GlobalLoader = GlobalLoader;
