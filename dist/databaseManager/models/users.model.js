"use strict";
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
    result["default"] = mod;
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
var sequelizer = __importStar(require("sequelize"));
var UsersModel = /** @class */ (function () {
    function UsersModel(DBconnection) {
        var usersSchema = {
            fullName: sequelizer.STRING,
            email: sequelizer.STRING,
            password: sequelizer.STRING,
            phoneNumber: sequelizer.STRING,
            loginType: sequelizer.STRING,
            profileImage: sequelizer.STRING,
            profileImageFilename: sequelizer.STRING,
            profileImageOriginalName: sequelizer.STRING,
            gender: sequelizer.ENUM('MALE', 'FEMALE', 'OTHER'),
            status: {
                type: sequelizer.INTEGER,
                defaultValue: 1
            },
            userOtp: {
                type: sequelizer.STRING,
            },
            forgotPasswordOtp: {
                type: sequelizer.STRING,
            },
            mobileVerifyStatus: {
                type: sequelizer.INTEGER,
                defaultValue: 1
            },
        };
        this.users = DBconnection.define('users', usersSchema, {
            indexes: [
                {
                    unique: true,
                    fields: ['email', 'phoneNumber']
                }
            ]
        });
    }
    return UsersModel;
}());
exports.default = UsersModel;
