"use strict";
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
    result["default"] = mod;
    return result;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
require("dotenv").config();
var https = __importStar(require("https"));
var loaders_1 = __importDefault(require("./loaders"));
var minimist = require("minimist");
var debug = require("debug");
var nodePort = process.env.PORT || 3000;
var port = normalizePort(nodePort);
var fs = require("fs");
var privateKey = fs.readFileSync("/etc/nginx/ssl_cert/private.key", "utf8");
var certificate = fs.readFileSync("/etc/nginx/ssl_cert/certificate.crt", "utf8");
var credentials = { key: privateKey, cert: certificate };
//let server: http.Server = http.createServer(app);
var server = https.createServer(credentials, loaders_1.default);
function normalizePort(val) {
    var port = parseInt(val, 10);
    if (isNaN(port)) {
        return val;
    }
    if (port >= 0) {
        return port;
    }
    return false;
}
function onError(error) {
    if (error.syscall !== "listen") {
        throw error;
    }
    var bind = typeof port === "string" ? "Pipe " + port : "Port " + port;
    // handle specific listen errors with friendly messages
    switch (error.code) {
        case "EACCES":
            console.error(bind + " requires elevated privileges");
            process.exit(1);
            break;
        case "EADDRINUSE":
            console.error(bind + " is already in use");
            process.exit(1);
            break;
        default:
            throw error;
    }
}
function onListening() {
    var addr = server.address();
    var bind = typeof addr === "string" ? "pipe " + addr : "port " + addr.port;
    debug("Listening on " + bind);
    console.log("Server started on port", port);
}
var tasks = {
    webServerStart: function () {
        server.listen(port);
        server.on("error", onError);
        server.on("listening", onListening);
        return true;
    },
    webServerStop: function () {
        return new Promise(function (resolve, reject) {
            server.close();
            resolve(loaders_1.default);
        });
    }
};
tasks.webServerStart();
(function () {
    var cliArgs = minimist(process.argv.slice(2));
    console.log(cliArgs, tasks.hasOwnProperty(cliArgs.start));
    if (cliArgs.hasOwnProperty("start") && tasks.hasOwnProperty(cliArgs.start)) {
        return tasks[cliArgs.start]();
    }
    else {
        console.log("unknown cli arguments");
    }
})();
