"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.PORT = process.env.PORT || 3000;
exports.DB_HOST = "127.0.0.1";
exports.DATABASE = "zomato_poc";
exports.DB_USERNAME = "root";
exports.DB_PASS = "root";
