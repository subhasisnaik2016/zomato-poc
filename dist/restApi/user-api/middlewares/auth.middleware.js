"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var user_repository_1 = __importDefault(require("../../../repositories/user.repository"));
var jwt = require("jsonwebtoken");
var md5 = require("md5");
var AuthMiddleware = /** @class */ (function () {
    function AuthMiddleware() {
    }
    /**
     * Middleware to validate login route
     * @param req Request Object
     * @param res Response Object
     * @param next NextFunction
     */
    AuthMiddleware.prototype.verifyLoggedIn = function (req, res, next) {
        return __awaiter(this, void 0, void 0, function () {
            var app, appHelper, databaseManager, secret, repository, email, password, result, token;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        app = req.app;
                        appHelper = app.globals.appHelpers;
                        databaseManager = app.globals.databaseManager;
                        secret = app.globals.secret;
                        repository = new user_repository_1.default(res, databaseManager.users);
                        email = "";
                        password = "";
                        if (typeof req.body.email === "undefined" || req.body.email === "") {
                            return [2 /*return*/, appHelper.sendResponse(res, 400, "email is empty")];
                        }
                        if (typeof req.body.password === "undefined" || req.body.password === "") {
                            return [2 /*return*/, appHelper.sendResponse(res, 400, "password is empty")];
                        }
                        password = md5(req.body.password);
                        email = req.body.email;
                        return [4 /*yield*/, repository.findOne({ email: email, password: password })];
                    case 1:
                        result = _a.sent();
                        if (result.success) {
                            token = jwt.sign({ userId: result.data.id, usertype: result.data.loginType }, secret);
                            req.token = token;
                            req.authUser = result.data;
                            next();
                        }
                        return [2 /*return*/, appHelper.sendResponse(res, 400, "Username or password is incorrect")];
                }
            });
        });
    };
    /**
      * Middleware to validate signup route
      * @param req Request Object
      * @param res Response Object
      * @param next NextFunction
      */
    AuthMiddleware.prototype.verifySignup = function (req, res, next) {
        return __awaiter(this, void 0, void 0, function () {
            var app, databaseManager, appHelper, emailRegexp, repository, Op, userExistfilter, checkUserExist;
            var _a;
            return __generator(this, function (_b) {
                switch (_b.label) {
                    case 0:
                        app = req.app;
                        databaseManager = app.globals.databaseManager;
                        appHelper = app.globals.appHelpers;
                        emailRegexp = /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$/;
                        repository = new user_repository_1.default(res, databaseManager.users);
                        if (!req.body.phoneNumber || req.body.phoneNumber == "") {
                            return [2 /*return*/, appHelper.sendResponse(res, 400, "Please enter valid phoneNumber")];
                        }
                        else if (!req.body.fullName || req.body.fullName == "") {
                            return [2 /*return*/, appHelper.sendResponse(res, 400, "Please enter fullName")];
                        }
                        else if (!req.body.email || req.body.email == "") {
                            return [2 /*return*/, appHelper.sendResponse(res, 400, "Please enter email")];
                        }
                        else if (!req.body.password || req.body.password == "") {
                            return [2 /*return*/, appHelper.sendResponse(res, 400, "Please enter password")];
                        }
                        else if (!emailRegexp.test(req.body.email)) {
                            return [2 /*return*/, appHelper.sendResponse(res, 400, "Not a valid email")];
                        }
                        if (!req.body.phoneNumber.startsWith("+")) {
                            req.body.phoneNumber = "+91" + req.body.phoneNumber;
                        }
                        Op = databaseManager.sequelize.Op;
                        userExistfilter = (_a = {
                                mobileVerifyStatus: 1
                            },
                            _a[Op.or] = [
                                { email: req.body.email },
                                { phoneNumber: req.body.phoneNumber }
                            ],
                            _a);
                        return [4 /*yield*/, repository.findOne(userExistfilter)];
                    case 1:
                        checkUserExist = _b.sent();
                        if (checkUserExist.success) {
                            return [2 /*return*/, appHelper.sendResponse(res, 400, "User credentials already exist")];
                        }
                        /** const prevUserfilter = {
                           mobileVerifyStatus: 0,
                           [Op.or]: [
                             { email: req.body.email },
                             { phoneNumber: req.body.phoneNumber }
                           ]
                         }
                         const checkPrevExist = await repository.findOne(prevUserfilter);
                     
                         if (checkPrevExist.success) {
                           req.previuosData = checkPrevExist.data;
                         }
                         */
                        next();
                        return [2 /*return*/];
                }
            });
        });
    };
    ;
    return AuthMiddleware;
}());
exports.AuthMiddleware = AuthMiddleware;
