"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var express_1 = require("express");
var auth_controller_1 = require("../controllers/auth.controller");
var auth_middleware_1 = require("../middlewares/auth.middleware");
var AuthRoute = /** @class */ (function () {
    function AuthRoute() {
        this.middleware = new auth_middleware_1.AuthMiddleware();
        this.authController = new auth_controller_1.AuthController();
        this.router = express_1.Router();
        this.routes();
    }
    AuthRoute.prototype.routes = function () {
        this.router.post("/login", this.middleware.verifyLoggedIn, this.authController.login);
        this.router.post("/signup", this.middleware.verifySignup, this.authController.signUp);
    };
    return AuthRoute;
}());
exports.default = AuthRoute;
