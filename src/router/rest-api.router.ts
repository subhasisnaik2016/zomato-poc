import { Router, Request, Response, NextFunction } from "express";
import AuthRoute from "../restApi/user-api/routes/auth.route";

export default class RestApiRouters {
  public router: Router;

  constructor() {
    this.router = Router();
    this.routes();
  }

  private routes(): any {
    this.router.get("/", async (req: Request, res: Response, next: NextFunction) => {
        res.send({ message: "Zomato POC API V1" });
      }
    );

    this.router.use("/auth", new AuthRoute().router);
  }
}
