import BaseRepository from './base.repository'

export default class UserRepository extends BaseRepository {

    constructor(response: Response, dbModel: any, databaseManager?: any) {
        super(response, dbModel, databaseManager);
    }
}