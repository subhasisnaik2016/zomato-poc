import AppHelpers from "../helpers";
import { DBResponse } from "../interfaces";

export default class BaseRepository {
    protected dbModel: any;
    protected databaseManager: any;
    protected appHelper: AppHelpers;
    protected response: any;

    constructor(response: Response, dbModel: any, databaseManager?: any) {
        this.appHelper = new AppHelpers();
        this.dbModel = dbModel;
        this.databaseManager = databaseManager;
        this.response = response;
    }

    findAll() { }

    async findOne(where: any): Promise<DBResponse> {
        try {
            let error, user;
            [error, user] = await this.appHelper.to(
                this.dbModel.findOne({
                    where
                })
            );
            if (error) {
                this.sendErrorResponse(error);
            }
            if (!user) {
                return { success: false };
            }
            return { success: true, data: user };
        } catch (e) {
            return { success: false };
        }


    }

    async create(data: any) {
        try {
            let error, success;
            [error, success] = await this.dbModel.create(data);
            if (error) {
                this.sendErrorResponse(error);
            }
            if (error) {
                return { success: false };
            }
            return { success: true, data: success };
        } catch (e) {
            return { success: false };
        }
     }

    update(id: number) { }

    delete(id: number) { }

    private sendErrorResponse(error: any) {
        let app: any = this.response.app;
        let appHelper: AppHelpers = app.globals.appHelpers;
        return appHelper.sendResponse(
            this.response,
            500,
            "Something went wrong",
            {},
            error
        );
    }

}