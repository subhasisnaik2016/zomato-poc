import { Response } from "express";

export default class AppHelpers {
  constructor() { }

  sendResponse(
    res: Response,
    statusCode: number,
    message: string = "",
    data: any = {},
    error?: any
  ) {
    if (statusCode >= 200 && statusCode < 400) {
      return res.status(statusCode).send({ status: true, message, data });
    } else {
      return res.status(statusCode).send({ status: false, message, data, error });
    }
  };

  to = (promise: any) => {
    return promise
      .then((data: any) => {
        return [null, data];
      })
      .catch((err: any) => {
        return [err, null];
      });
  };
}
