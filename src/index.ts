require("dotenv").config();
import * as http from "http";
import * as https from "https";
import app from "./loaders";
const minimist = require("minimist");
const debug = require("debug");

const nodePort: any = process.env.PORT || 3000;

const port: string | number | false = normalizePort(nodePort);

const fs = require("fs");

const privateKey = fs.readFileSync("/etc/nginx/ssl_cert/private.key", "utf8");
const certificate = fs.readFileSync("/etc/nginx/ssl_cert/certificate.crt", "utf8");
const credentials = { key: privateKey, cert: certificate };

//let server: http.Server = http.createServer(app);
let server = https.createServer(credentials, app);

function normalizePort(val: string): string | number | false {
  let port = parseInt(val, 10);

  if (isNaN(port)) {
    return val;
  }

  if (port >= 0) {
    return port;
  }

  return false;
}

function onError(error: any) {
  if (error.syscall !== "listen") {
    throw error;
  }

  var bind = typeof port === "string" ? "Pipe " + port : "Port " + port;

  // handle specific listen errors with friendly messages
  switch (error.code) {
    case "EACCES":
      console.error(bind + " requires elevated privileges");
      process.exit(1);
      break;
    case "EADDRINUSE":
      console.error(bind + " is already in use");
      process.exit(1);
      break;
    default:
      throw error;
  }
}

function onListening() {
  let addr: any = server.address();
  let bind = typeof addr === "string" ? "pipe " + addr : "port " + addr.port;
  debug("Listening on " + bind);
  console.log("Server started on port", port);
}

const tasks: any = {
  webServerStart: () => {
    server.listen(port);
    server.on("error", onError);
    server.on("listening", onListening);
    return true;
  },
  webServerStop: () => {
    return new Promise((resolve, reject) => {
      server.close();
      resolve(app);
    });
  }
};
tasks.webServerStart();

(function() {
  let cliArgs = minimist(process.argv.slice(2));
  console.log(cliArgs, tasks.hasOwnProperty(cliArgs.start));
  if (cliArgs.hasOwnProperty("start") && tasks.hasOwnProperty(cliArgs.start)) {
    return tasks[cliArgs.start]();
  } else {
    console.log("unknown cli arguments");
  }
})();
