import * as express from "express";
import RestApiRouters from "../router/rest-api.router";

export class RouteLoader {
  public app: express.Application;
  constructor(app: express.Application) {
    this.app = app;
  }

  /**
   * Configure application
   *
   * @class Server
   * @method config
   */
  public load() {
    this.app.use("/api/v1", new RestApiRouters().router);
  }
}
