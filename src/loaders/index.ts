import { ExpressLoader } from "./express.loader";
import { RouteLoader } from "./route.loader";
import { GlobalLoader } from "./global.loader";
const express = require("express");

class Loader {
  public app: any;
  public expressLoader: ExpressLoader;
  public routeLoader: RouteLoader;
  public globalLoader: GlobalLoader;

  constructor() {
    this.app = express();
    this.expressLoader = new ExpressLoader(this.app);
    this.routeLoader = new RouteLoader(this.app);
    this.globalLoader = new GlobalLoader(this.app);
    this.init();
  }

  public init() {
    this.expressLoader.load();
    this.routeLoader.load();
  }
}

export default new Loader().app;
