import * as express from "express";

import * as bodyParser from "body-parser";
const cors = require("cors");

export class ExpressLoader {
  public app: express.Application;
  constructor(app: express.Application) {
    this.app = app;
  }

  /**
   * Configure application
   *
   * @class Server
   * @method config
   */
  public load() {
    this.app.use(cors());

    //use json form parser middlware
    this.app.use(bodyParser.json());

    //use query string parser middlware
    this.app.use(
      bodyParser.urlencoded({
        extended: true,
        limit: "50mb"
      })
    );
  }
}
