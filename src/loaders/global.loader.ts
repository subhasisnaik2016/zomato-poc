import * as express from "express";
import RestApiRouters from "../router/rest-api.router";
import DatabaseManager from "../databaseManager";
import AppHelpers from "../helpers";

export class GlobalLoader {
  public app: express.Application;
  constructor(app: any) {
    this.app = app;
    app.globals = {};
    app.globals.databaseManager = new DatabaseManager();
    app.globals.appHelpers = new AppHelpers();
    app.globals.secret = 'jkhjhsuihfweyr274y4f438gbhjsret84tgr';
  }
}
