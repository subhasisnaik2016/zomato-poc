import { NextFunction } from "express";
import AppHelpers from "../../../helpers";
import DatabaseManager from "../../../databaseManager";
import UserRepository from "../../../repositories/user.repository";

let jwt = require("jsonwebtoken");
let md5 = require("md5");

export class AuthMiddleware {

  constructor() {

  }

  /**
   * Middleware to validate login route
   * @param req Request Object
   * @param res Response Object
   * @param next NextFunction
   */
  async verifyLoggedIn(req: any, res: Response | any, next: NextFunction) {
    let app: any = req.app;
    const appHelper: AppHelpers = app.globals.appHelpers;
    const databaseManager: DatabaseManager = app.globals.databaseManager;
    const secret = app.globals.secret;

    const repository: UserRepository = new UserRepository(res, databaseManager.users);

    let email = "";
    let password = "";


    if (typeof req.body.email === "undefined" || req.body.email === "") {
      return appHelper.sendResponse(res, 400, "email is empty");
    }
    if (typeof req.body.password === "undefined" || req.body.password === "") {
      return appHelper.sendResponse(res, 400, "password is empty");
    }

    password = md5(req.body.password);
    email = req.body.email;
    const result = await repository.findOne({ email, password });

    if (result.success) {
      const token = jwt.sign({ userId: result.data.id, usertype: result.data.loginType }, secret);
      req.token = token;
      req.authUser = result.data;
      next();
    }

    return appHelper.sendResponse(res, 400, "Username or password is incorrect");
  }


  /**
    * Middleware to validate signup route
    * @param req Request Object
    * @param res Response Object
    * @param next NextFunction
    */
  async verifySignup(req: any, res: Response | any, next: NextFunction) {
    const app: any = req.app;
    const databaseManager: DatabaseManager = app.globals.databaseManager;
    const appHelper: AppHelpers = app.globals.appHelpers;
    const emailRegexp = /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$/;

    const repository: UserRepository = new UserRepository(res, databaseManager.users);


    if (!req.body.phoneNumber || req.body.phoneNumber == "") {
      return appHelper.sendResponse(
        res,
        400,
        "Please enter valid phoneNumber"
      );
    } else if (!req.body.fullName || req.body.fullName == "") {
      return appHelper.sendResponse(res, 400, "Please enter fullName");
    } else if (!req.body.email || req.body.email == "") {
      return appHelper.sendResponse(res, 400, "Please enter email");
    } else if (!req.body.password || req.body.password == "") {
      return appHelper.sendResponse(res, 400, "Please enter password");
    } else if (!emailRegexp.test(req.body.email)) {
      return appHelper.sendResponse(res, 400, "Not a valid email");
    }

    if (!req.body.phoneNumber.startsWith("+")) {
      req.body.phoneNumber = "+91" + req.body.phoneNumber;
    }


    const Op = databaseManager.sequelize.Op;

    const userExistfilter = {
      mobileVerifyStatus: 1,
      [Op.or]: [
        { email: req.body.email },
        { phoneNumber: req.body.phoneNumber }
      ]
    }
    const checkUserExist = await repository.findOne(userExistfilter);

    if (checkUserExist.success) {
      return appHelper.sendResponse(res, 400, "User credentials already exist");
    }

    /** const prevUserfilter = {
       mobileVerifyStatus: 0,
       [Op.or]: [
         { email: req.body.email },
         { phoneNumber: req.body.phoneNumber }
       ]
     }
     const checkPrevExist = await repository.findOne(prevUserfilter);
 
     if (checkPrevExist.success) {
       req.previuosData = checkPrevExist.data;
     }
     */

    next();
  };
}
