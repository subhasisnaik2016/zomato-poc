import UserRepository from "../../../repositories/user.repository";
import DatabaseManager from "../../../databaseManager";
const md5 = require("md5");

export default class AuthService {

    constructor() {
    }

    login(request: Request | any) {
        const { token, authUser } = request;
        const resData = {
            phone_verified: false,
            username: authUser.email
        };

        if (authUser.mobileVerifyStatus) {
            const resData = {
                phone_verified: true,
                token: token,
                user: authUser
            };
            return { status: 200, message: "Login success", data: resData };
        }
        return { status: 200, message: "Login success", data: resData };
    }

    async signUp(request: Request | any, response: any) {
        let app: any = request.app;
        const databaseManager: DatabaseManager = app.globals.databaseManager;
        const repository: UserRepository = new UserRepository(response, databaseManager.users);
        let userData = {
            fullName: request.body.fullName,
            email: request.body.email,
            phoneNumber: request.body.phoneNumber,
            password: md5(request.body.password),
            loginType: "user"
        };

        const result = await repository.create(userData);

        if (result.success) {
            return { status: 200, message: "Registration success", data: result.data };
        }
        return { status: 400, message: "Registration failed", data: {} };
       
    }
}