import { Router, Request, Response, NextFunction } from "express";
import { AuthController } from "../controllers/auth.controller";
import { AuthMiddleware } from "../middlewares/auth.middleware";

export default class AuthRoute {
  public router: Router;
  private middleware: AuthMiddleware;
  private authController: AuthController;

  constructor() {
    this.middleware = new AuthMiddleware();
    this.authController = new AuthController();
    this.router = Router();
    this.routes();
  }

  private routes(): any {
    this.router.post("/login", this.middleware.verifyLoggedIn, this.authController.login);
    this.router.post("/signup", this.middleware.verifySignup, this.authController.signUp);
  }
}
