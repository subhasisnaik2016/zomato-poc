import { NextFunction } from "express";
import AppHelpers from "../../../helpers";
import AuthService from '../services/auth.service';

export class AuthController {

  async login(req: any, res: Response | any, next: NextFunction) {
    const app: any = req.app;
    const appHelper: AppHelpers = app.globals.appHelpers;
    const service: AuthService = new AuthService();
    let responseData = service.login(req);

    return appHelper.sendResponse(res, responseData.status, responseData.message, responseData.data);
  }

  async signUp(req: any, res: Response | any, next: NextFunction) {
    const app: any = req.app;
    const appHelper: AppHelpers = app.globals.appHelpers;
    const service: AuthService = new AuthService();
    let responseData = await service.signUp(req, res);

    return appHelper.sendResponse(res, responseData.status, responseData.message, responseData.data);
  }
}
