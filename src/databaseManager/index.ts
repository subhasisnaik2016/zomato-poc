import * as constants from "../config/constants";
import UsersModel from "./models/users.model";
const sequelizer = require("sequelize");

export default class DatabaseManager {
  /**
   * Models
   */
  sequelize: any;
  users: any;

  constructor() {
    this.modelSetup();
  }

  private modelSetup = async () => {
    try {
      const sequelize = new sequelizer(
        constants.DATABASE,
        constants.DB_USERNAME,
        constants.DB_PASS,
        {
          host: constants.DB_HOST,
          dialect: "mysql",
          operatorsAliases: false,
          logging: console.log,
          pool: {
            max: 50,
            min: 0,
            acquire: 70000,
            idle: 70000
          }
        }
      );

      await sequelize
        .authenticate()
        .then(() => {})
        .catch((err: any) => {
          console.log("Unable to connect to the database:", err);
        });

      this.sequelize = sequelize;
      this.users = new UsersModel(sequelize).users;
      await sequelize.query("SET FOREIGN_KEY_CHECKS = 0");
      // await sequelize.sync({ alter: true });
      await sequelize.query("SET FOREIGN_KEY_CHECKS = 1");
    } catch (e) {
      console.error(e);
    }
  };
}
