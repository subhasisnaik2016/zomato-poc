import * as sequelizer from 'sequelize';
export default class UsersModel {
    users: any;
    constructor(DBconnection: any) {
        const usersSchema = {

            fullName: sequelizer.STRING,
            email: sequelizer.STRING,
            password: sequelizer.STRING,
            phoneNumber: sequelizer.STRING,
            loginType: sequelizer.STRING,
            profileImage: sequelizer.STRING,
            profileImageFilename: sequelizer.STRING,
            profileImageOriginalName: sequelizer.STRING,
            gender: sequelizer.ENUM('MALE', 'FEMALE', 'OTHER'),
            status: {
                type: sequelizer.INTEGER,
                defaultValue: 1
            },
            userOtp: {
                type: sequelizer.STRING,
            },
            forgotPasswordOtp: {
                type: sequelizer.STRING,
            },
            mobileVerifyStatus: {
                type: sequelizer.INTEGER,
                defaultValue: 1
            },
        };
        this.users = DBconnection.define(
            'users',
            usersSchema,
            {
                indexes: [
                    {
                        unique: true,
                        fields: ['email', 'phoneNumber']
                    }
                ]
            }
        );

    }
}