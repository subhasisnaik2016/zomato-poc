export interface DBResponse {
    success: boolean,
    data?: any
}